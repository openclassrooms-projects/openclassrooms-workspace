# OpenClassrooms Workspace

## Getting started

```bash
git clone --recurse-submodules git@gitlab.com:openclassrooms-projects/openclassrooms-workspace.git
```
### Add sub module to workspace
```bash
git submodule add -b main <repo> <path/to/dest>
```